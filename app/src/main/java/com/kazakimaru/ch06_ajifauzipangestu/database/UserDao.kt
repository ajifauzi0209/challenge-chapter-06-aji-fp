package com.kazakimaru.ch06_ajifauzipangestu.database

import androidx.room.*

@Dao
interface UserDao {
    // Cek ketersediaan username pada saat register
    @Query("SELECT * FROM User WHERE username = :username")
    fun checkRegisteredUsername(username: String): List<User>

    // Cek ketersediaan email pada saat register
    @Query("SELECT * FROM User WHERE email = :email")
    fun checkRegisteredEmail(email: String): List<User>

    // Cek ketersediaan email and password pada saat login
    @Query("SELECT * FROM User WHERE email = :email AND password = :password")
    fun checkRegisteredUser(email: String, password: String): List<User>

    // Mendapatkan username berdasarkan email login
    @Query("SELECT * FROM User WHERE email = :email")
    fun getUsernameByEmail(email: String): User

    // Mendapatkan UserDetail
    @Query("SELECT * FROM UserDetail WHERE username = :username")
    fun getAllUserDetail(username: String): List<UserDetail>

    // Mendapatkan salah satu UserDetail
    @Query("SELECT * FROM UserDetail WHERE username = :username")
    fun getAUserDetail(username: String): UserDetail

    // Insert UserDetail
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUserDetail(userDetail: UserDetail): Long

    // Update userdetail yang sudah ada berdasarkan ...
    @Query("UPDATE UserDetail SET nama_lengkap = :nama_lengkap, tgl_lahir = :tgl_lahir, alamat = :alamat WHERE username = :username")
    fun updateUserDetail(username: String, nama_lengkap: String, tgl_lahir: String, alamat: String): Int

    // Insert ke Database
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User): Long

}