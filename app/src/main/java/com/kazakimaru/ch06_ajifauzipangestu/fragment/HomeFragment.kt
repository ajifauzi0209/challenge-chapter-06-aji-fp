package com.kazakimaru.ch06_ajifauzipangestu.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.kazakimaru.ch06_ajifauzipangestu.BuildConfig
import com.kazakimaru.ch06_ajifauzipangestu.R
import com.kazakimaru.ch06_ajifauzipangestu.adapter.TMDBAdapter
import com.kazakimaru.ch06_ajifauzipangestu.databinding.FragmentHomeBinding
import com.kazakimaru.ch06_ajifauzipangestu.helper.viewModelsFactory
import com.kazakimaru.ch06_ajifauzipangestu.model.Result
import com.kazakimaru.ch06_ajifauzipangestu.model.Status
import com.kazakimaru.ch06_ajifauzipangestu.repo.DatastoreManager
import com.kazakimaru.ch06_ajifauzipangestu.repo.MovieRepo
import com.kazakimaru.ch06_ajifauzipangestu.service.TMDBApiService
import com.kazakimaru.ch06_ajifauzipangestu.service.TMDBClient
import com.kazakimaru.ch06_ajifauzipangestu.viewmodel.DatastoreViewModel
import com.kazakimaru.ch06_ajifauzipangestu.viewmodel.MovieViewModel


class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var tmdbAdapter: TMDBAdapter

    private val pref: DatastoreManager by lazy { DatastoreManager(requireContext()) }
    private val datastoreViewModel: DatastoreViewModel by viewModelsFactory { DatastoreViewModel(pref) }

    private val tmdbApiService: TMDBApiService by lazy { TMDBClient.instance }
    private val movieRepo: MovieRepo by lazy { MovieRepo(tmdbApiService) }
    private val movieViewModel: MovieViewModel by viewModelsFactory { MovieViewModel(movieRepo) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        moveToProfile()
        showUsername()
        observeMovie()
    }

    private fun initRecyclerView() {
        tmdbAdapter = TMDBAdapter { id_momovie,pilem: Result ->
            val bandel = Bundle()
            bandel.putInt("aidi_pilem", id_momovie)
            findNavController().navigate(R.id.action_homeFragment_to_detailMovieFragment, bandel)
        }
        binding.apply {
            rvData.adapter = tmdbAdapter
            rvData.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun moveToProfile() {
        binding.btnAccount.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_profileFragment)
        }
    }

    private fun showUsername() {
        datastoreViewModel.getUsername().observe(viewLifecycleOwner) {
            binding.txtWelcomeUser.text = "Welcome, $it"
        }
    }

    private fun observeMovie() {
        movieViewModel.getAllMoviePopular(BuildConfig.API_KEY).observe(viewLifecycleOwner) {
            when (it.status) {
                Status.LOADING -> {
                    // Handle ketika data loading
                    // progress bar muncul
                    binding.pb.isVisible = true
                }
                Status.SUCCESS -> {
                    // Handle ketika data success
                    // progress bar ilang
                    binding.pb.isVisible = false
                    tmdbAdapter.updateDataRecycler(it.data)
                }
                Status.ERROR -> {
                    // Handle ketika data error
                    // progress bar ilang
                    binding.pb.isVisible = false
                    Toast.makeText(requireContext(), "Error Guys", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}