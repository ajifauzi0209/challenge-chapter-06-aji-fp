package com.kazakimaru.ch06_ajifauzipangestu.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.kazakimaru.ch06_ajifauzipangestu.MainActivity
import com.kazakimaru.ch06_ajifauzipangestu.R
import com.kazakimaru.ch06_ajifauzipangestu.databinding.FragmentLoginBinding
import com.kazakimaru.ch06_ajifauzipangestu.helper.viewModelsFactory
import com.kazakimaru.ch06_ajifauzipangestu.repo.DatastoreManager
import com.kazakimaru.ch06_ajifauzipangestu.repo.UserRepo
import com.kazakimaru.ch06_ajifauzipangestu.viewmodel.DatastoreViewModel
import com.kazakimaru.ch06_ajifauzipangestu.viewmodel.LoginViewModel


class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private val userRepo: UserRepo by lazy { UserRepo(requireContext()) }
    private val loginViewModel: LoginViewModel by viewModelsFactory { LoginViewModel(userRepo) }

    private val pref: DatastoreManager by lazy { DatastoreManager(requireContext()) }
    private val datastoreViewModel: DatastoreViewModel by viewModelsFactory { DatastoreViewModel(pref) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        moveToRegister()
        doLogin()
        observeData()
    }

    private fun moveToRegister() {
        binding.txtDaftar.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }

    private fun doLogin() {
        binding.btnLogin.setOnClickListener {
            // Get value dari EditText
            val etEmail = binding.editEmail.editText?.text.toString()
            val etPassword = binding.editPassword.editText?.text.toString()
            // Validasi inputan kosong
            if (loginValidation(etEmail, etPassword)) {
                // Jalankan fungsi di ViewModel
                loginViewModel.loginUser(etEmail, etPassword)
            }
        }
    }

    private fun loginValidation(email: String, password: String): Boolean {
        var result = true
        if (email.isEmpty()) { // jika kosong
            binding.editEmail.error = "Email tidak boleh kosong!"
            result = false
        } else {
            binding.editEmail.isErrorEnabled = false
        }

        if (password.isEmpty()) { // jika kosong
            binding.editPassword.error = "Password tidak boleh kosong!"
            result = false
        }  else {
            binding.editPassword.isErrorEnabled = false
        }

        return result
    }
    private fun observeData() {
        loginViewModel.statusLogin.observe(viewLifecycleOwner) {
            if (it == false) { // jika gagal
                Toast.makeText(requireContext(), "Email atau Password salah!", Toast.LENGTH_SHORT).show()
            } else { // jika berhasil
                // Simpan Login State ke Datastore
                datastoreViewModel.saveLoginState(it) // True
                // Munculkan toast 'Berhasil Login'
                Toast.makeText(requireContext(), "Berhasil Login", Toast.LENGTH_SHORT).show()
                // Pindah screen ke HomeFragment (berada di MainActivity)
                startActivity(Intent(requireContext(), MainActivity::class.java))
                requireActivity().finish()
            }
        }

        loginViewModel.username.observe(viewLifecycleOwner) {
            datastoreViewModel.saveUsername(it)
        }

    }

}