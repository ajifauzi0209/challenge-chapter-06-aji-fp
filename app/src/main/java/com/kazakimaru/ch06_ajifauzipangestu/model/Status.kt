package com.kazakimaru.ch06_ajifauzipangestu.model


/**
 * Created by nurrahmanhaadii on 14,May,2022
 */
enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
