package com.kazakimaru.ch06_ajifauzipangestu.repo

import com.kazakimaru.ch06_ajifauzipangestu.service.TMDBApiService

class MovieRepo(private val tmdbApiService: TMDBApiService) {
    suspend fun getMovie(apiKey: String) = tmdbApiService.getAllMovie(apiKey)
    suspend fun getMovieDetail(movieId: Int, apiKey: String) = tmdbApiService.getDetailMovie(movieId, apiKey)
}