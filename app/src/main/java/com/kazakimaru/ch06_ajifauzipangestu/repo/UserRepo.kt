package com.kazakimaru.ch06_ajifauzipangestu.repo

import android.content.Context
import com.kazakimaru.ch06_ajifauzipangestu.database.User
import com.kazakimaru.ch06_ajifauzipangestu.database.UserDatabase
import com.kazakimaru.ch06_ajifauzipangestu.database.UserDetail
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UserRepo(context: Context) {
    private val userDB = UserDatabase.getInstance(context)

    suspend fun checkRegisteredkUsername(username: String) = withContext(Dispatchers.IO) {
        userDB?.userDao()?.checkRegisteredUsername(username)
    }

    suspend fun checkRegisteredEmail(email: String) = withContext(Dispatchers.IO) {
        userDB?.userDao()?.checkRegisteredEmail(email)
    }

    suspend fun checkRegisteredUser(email: String, password: String) = withContext(Dispatchers.IO) {
        userDB?.userDao()?.checkRegisteredUser(email, password)
    }

    suspend fun getUsernameByMail(email: String) = withContext(Dispatchers.IO) {
        userDB?.userDao()?.getUsernameByEmail(email)
    }

    suspend fun getUserDetail(username: String) = withContext(Dispatchers.IO) {
        userDB?.userDao()?.getAllUserDetail(username)
    }

    suspend fun getAUser(username: String) = withContext(Dispatchers.IO) {
        userDB?.userDao()?.getAUserDetail(username)
    }

    suspend fun insertUserDetail(userDetail: UserDetail) = withContext(Dispatchers.IO) {
        userDB?.userDao()?.insertUserDetail(userDetail)
    }

    suspend fun updateUserProfile(username: String, nama_lengkap: String, tgl_lahir: String, alamat: String) = withContext(Dispatchers.IO) {
        userDB?.userDao()?.updateUserDetail(username, nama_lengkap, tgl_lahir, alamat)
    }

    suspend fun insertUser(user: User) = withContext(Dispatchers.IO) {
        userDB?.userDao()?.insertUser(user)
    }

}