package com.kazakimaru.ch06_ajifauzipangestu.service

import com.kazakimaru.ch06_ajifauzipangestu.model.Result
import com.kazakimaru.ch06_ajifauzipangestu.model.MoviePopularResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TMDBApiService {
    @GET("movie/popular")
    suspend fun getAllMovie(
        @Query("api_key") apiKey: String
    ): MoviePopularResponse

    @GET("movie/{movie_id}")
    suspend fun getDetailMovie(
        @Path("movie_id") movieId: Int,
        @Query("api_key") apiKey: String
    ): Result
}