package com.kazakimaru.ch06_ajifauzipangestu.viewmodel

import androidx.lifecycle.*
import com.kazakimaru.ch06_ajifauzipangestu.repo.DatastoreManager
import kotlinx.coroutines.launch

class DatastoreViewModel(private val pref: DatastoreManager): ViewModel() {

    fun saveLoginState(value: Boolean) {
        viewModelScope.launch {
            pref.saveLoginStateToDataStore(value)
        }
    }

    fun getLoginState() : LiveData<Boolean> {
        return pref.readLoginStateFromDataStore().asLiveData()
    }

    fun saveUsername(value: String) {
        viewModelScope.launch {
            pref.saveUsernameToDataStore(value)
        }
    }

    fun getUsername() : LiveData<String> {
        return pref.readUsernameFromDataStore().asLiveData()
    }

    fun saveImage(value: String) {
        viewModelScope.launch {
            pref.saveImageToDataStore(value)
        }
    }

    fun getImage() : LiveData<String> {
        return pref.readImageFromDataStore().asLiveData()
    }

    fun deleteAllData() {
        viewModelScope.launch {
            pref.removeFromDataStore()
        }
    }
}