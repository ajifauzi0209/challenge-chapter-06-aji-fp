package com.kazakimaru.ch06_ajifauzipangestu.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.kazakimaru.ch06_ajifauzipangestu.model.Resource
import com.kazakimaru.ch06_ajifauzipangestu.repo.MovieRepo
import kotlinx.coroutines.Dispatchers

class MovieViewModel(private val movieRepo: MovieRepo): ViewModel() {

    fun getAllMoviePopular(apiKey: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(movieRepo.getMovie(apiKey)))
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))
        }
    }

    fun getMovieDetail(movieId: Int, apiKey: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(movieRepo.getMovieDetail(movieId, apiKey)))
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))
        }
    }
}